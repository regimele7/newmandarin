const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const morgan = require('morgan');
const userRouter = require('./routes/users');
const recipesRouter = require('./routes/recipes');

const db_username = 'test_user';
const db_password = 'password12345';

const app = express();

app.use(express.json());
app.use(cors());
app.use(morgan('dev'));

// Routet e userit
app.use("/auth", userRouter);

// Routet e recetave
app.use("/recipes", recipesRouter);

mongoose.connect(
  `mongodb+srv://${db_username}:${db_password}@recipes.m0pgnmd.mongodb.net/?retryWrites=true&w=majority`,
);

app.listen(3001, () => console.log("Server started"));
