const express = require('express');
const router = express.Router();
const {userRegister,userLogIn} = require('../controllers/userControllers')

router.post("/register", userRegister);

router.post("/login",userLogIn);

module.exports = router;
