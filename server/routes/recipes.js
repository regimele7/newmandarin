const express = require('express');
const router = express.Router();

const {
  getAllRecepies,
  getRecipeById,
  addNewRecipe,
  getSavedRecipes,
  saveRecipe,
  getIdSavedRecipes
} = require('../controllers/recipesControllers');

router.get("/", getAllRecepies);

router.post("/", addNewRecipe);

router.get("/:recipeId", getRecipeById);

// router.put("/", async (req, res) => {
//     res.send('Kerkesa per te updetuar recete u mor!')
// });

router.get("/savedRecipes/:userId", getSavedRecipes);

router.put("/saveRecipe", saveRecipe);

router.get("/savedRecipes/ids/:userId", getIdSavedRecipes);

module.exports = router;
