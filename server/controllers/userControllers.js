const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const UserModel = require('../models/Users');
const messages = require('../constants/auth_messages.json')

const userRegister = async (req, res) => {
    
    const { 
        username,
        password,
        name,
        last_name,
        birth_date
    } = req.body;

    if(username.length < 3){
        return res.status(403).json({
            message: messages.invalid_username
        });
    }

    if(password.length < 6){
        return res.status(403).json({
            message: messages.invalid_password
        });
    }

    const hashedPassword = await bcrypt.hash(password, 10);
    const user = await UserModel.findOne({ username });
    if (user) {
        return res.status(403).json({
            message: messages.duplicated_entry
        });
    }

    const newUser = new UserModel({
        username,
        password:hashedPassword,
        name,
        last_name,
        birth_date
    });

    await newUser.save();
    const token = jwt.sign({ user }, "secret");

    return res.status(200).json({
        message: messages.successfully_created,
        token: token
    });
}

const userLogIn =  async (req, res) => {
    const {username, password} = req.body;
    const user = await UserModel.findOne({ username });

    if(user){
        const isPasswordValid = await bcrypt.compare(password, user.password);
        if(!isPasswordValid){
            return res.status(401).json({
                message: messages.wrong_credentials
            })
        }
    }else{
        return res.status(401).json({
            message: messages.user_does_not_exist
        })
    }
    
    const token = jwt.sign({ user }, "secret");

    return res.status(200).json({
        message: messages.successfully_authenticated,
        token,
    })
}

module.exports = {
    userRegister,
    userLogIn
}



// export const verifyToken = (req, res, next) => {
//     const authHeader = req.headers.authorization;
//     if (authHeader) {
//       jwt.verify(authHeader, "secret", (err) => {
//         if (err) {
//           return res.sendStatus(403);
//         }
//         next();
//       });
//     } else {
//       res.sendStatus(401);
//     }
//   };