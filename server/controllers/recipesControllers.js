const RecipesModel = require('../models/Recipes');
const UserModel = require('../models/Users');

const getAllRecepies = async (req, res) => {
    try {
        const result = await RecipesModel.find({});
        res.status(200).json(result);
    } catch (error) {
        res.status(500).json(err);
    }
}

const addNewRecipe = async (req, res) => {

    const recipe = new RecipesModel({
        // _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        ingredients: req.body.ingredients,
        instructions: req.body.instructions,
        image_url: req.body.image_url,
        cookingTime: req.body.cookingTime,
        userOwner: req.body.userOwner,
    });
      console.log(recipe);
    
      try {
        const result = await recipe.save();
        res.status(201).json({
          createdRecipe: {
            name: result.name,
            image: result.image,
            ingredients: result.ingredients,
            instructions: result.instructions,
            _id: result._id,
          },
        });
      } catch (err) {
        res.status(500).json(err);
}}

const getRecipeById = async (req, res) => {
    try {
      const result = await RecipesModel.findById(req.params.recipeId);
      res.status(200).json(result);
    } catch (err) {
      res.status(500).json(err);
    }
  }

const getSavedRecipes = async (req, res) => {
    try {
      const user = await UserModel.findById(req.params.userId);
      const savedRecipes = await RecipesModel.find({
        _id: { $in: user.savedRecipes },
      });
  
      res.status(201).json({ savedRecipes });
    } catch (err) {
      console.log(err);
      res.status(500).json(err);
    }
  }

const saveRecipe = async (req, res) => {
    const recipe = await RecipesModel.findById(req.body.recipeID);
    const user = await UserModel.findById(req.body.userID);
    try {
      user.savedRecipes.push(recipe);
      await user.save();
      res.status(201).json({ savedRecipes: user.savedRecipes });
    } catch (err) {
      res.status(500).json(err);
    }
  }

const getIdSavedRecipes = async (req, res) => {
    try {
      const user = await UserModel.findById(req.params.userId);
      res.status(201).json({ savedRecipes: user?.savedRecipes });
    } catch (err) {
      console.log(err);
      res.status(500).json(err);
    }
  }

module.exports = {
    getAllRecepies,
    addNewRecipe,
    getRecipeById,
    getSavedRecipes,
    saveRecipe,
    getIdSavedRecipes
}