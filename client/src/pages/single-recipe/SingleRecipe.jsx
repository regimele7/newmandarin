import React, { useState, useEffect } from 'react'
import axios from 'axios';
import { useLocation } from "react-router-dom";

const SingleRecipe = () => {
    const { state } = useLocation();
    const [recipt_id, setRecipt_id] = useState(state.recipt_id);
    const [recipt, setRecipt] = useState();
  
    useEffect(() => {
        function fetchData() {
            axios.get(`http://localhost:3001/recipes/${recipt_id}`)
            .then(function (response) {
                return response.data;
            })
            .then(data => {
                setRecipt(data);
            })
            .catch(function (error) {
                console.log(error);
            });
        }

        fetchData();
    }, [])
    

    return (
        <div>
            {
                recipt._id
            }
        </div>
    )
}

export default SingleRecipe