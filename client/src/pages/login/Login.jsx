import React, { useState } from 'react'
import { useNavigate } from "react-router-dom"
import axios from 'axios'
import { useCookies } from "react-cookie"
import './login.css'

const Login = () => {
    const [_, setCookies] = useCookies(["name"]);
    const [userName, setUserName] = useState("");
    const [pass, setPass] = useState("");

    const navigate = useNavigate();

    const handleSubmit = (event) => {
        event.preventDefault();
        if (userName == '' || pass == '') {
            return
        }

        // Requesti per backun
        axios.post('http://localhost:3001/auth/login', {
            username: userName,
            password: pass,
        })
        .then(function (response) {
            console.log("Inside Axios success callback");
            console.log(response.data.data);
            if (response.data.data) {
                setCookies("token", response.data);
            }
            setCookies("userName", userName);
            setCookies("token", response.data.token);
            navigate("/home");
        })
        .catch(function (error) {
            alert('Kredencialet gabim');
            console.log(error);
        });
    }

    return (
        <div className='flex justify-center p-5 pb-[10%]'>
            <form className="wrapper">
                <h2>LOGIN</h2>
                <section className="group">
                    <input
                        type="text"
                        size="30"
                        className="input"
                        name="username"
                        required
                        onChange={(event) => setUserName(event.target.value)}
                    />
                    <label htmlFor="username" className="label">
                        Username
                    </label>
                </section>
                <section className="group">
                    <input
                        type="password"
                        minLength="8"
                        className="input"
                        name="password"
                        required
                        onChange={(event) => setPass(event.target.value)}
                    />
                    <label htmlFor="password" className="label">
                        Password
                    </label>
                </section>
                <button type="button" className="btn" onClick={handleSubmit}>
                    LOGIN
                </button>
                <span className="form_shadow"></span>
            </form>
        </div>
    )
}

export default Login