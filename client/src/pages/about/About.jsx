import React from 'react'
import './about.css'
import { IoIosArrowRoundDown } from "react-icons/io";

const About = () => {
  return (
    <div className='add-bg text-white font-bold p-[7%]'>
      <div className='flex justify-around'>
        <div className='text-[#66a0c0] text-[140px]'><h1>Receta <br /> gatimi</h1></div>
        <div>
          <div className='text-[22px] pt-[25%]'>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Labore, necessitatibus.</div>
          <br />
          <div className='flex justify-start pt-[5]'>
            <div className='p-5 rounded-lg mr-[15%] bg-[#222831]'>Guarantee off</div>
            <div className='flex rounded-lg items-center p-5 bg-[#222831]'><h2 className='pr-3'>Download</h2><IoIosArrowRoundDown /></div>
          </div>
        </div>
      </div>
      <div className='flex justify-around pt-[10%]'>
        <div>
          <h2>NEXT INTAKE</h2>
          <div className='w-[350px] h-[2px] bg-[#FFFFFF]'></div>
          <div>June 5 2023</div>
        </div>
        <div>
          <h2>NEXT INTAKE</h2>
          <div className='w-[350px] h-[2px] bg-[#FFFFFF]'></div>
          <div>June 5 2023</div>
        </div>
        <div>
          <h2>NEXT INTAKE</h2>
          <div className='w-[350px] h-[2px] bg-[#FFFFFF]'></div>
          <div>June 5 2023</div>
        </div>
      </div>
    </div>
  )
}

export default About