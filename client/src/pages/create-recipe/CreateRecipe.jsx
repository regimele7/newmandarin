import React, { useState } from 'react';
import { useCookies } from "react-cookie";
import { useNavigate } from "react-router-dom";
import axios from 'axios'
import { useJwt } from "react-jwt";
import './createRecipe.css';

const CreateRecipe = () => {
    const [cookies, setCookies] = useCookies(["name"]);
    const { decodedToken, isExpired } = useJwt(cookies.token);
    const navigator = useNavigate();
    const [recipe, setRecipe] = useState({
        name: "",
        desc: "",
        time: 0,
        ing: [""],
        ins: "",
        photo_url: "",
    });

    const handleChange = (event) => {
        const { name, value } = event.target;
        setRecipe({ ...recipe, [name]: value });
    };

    const handleIngredientChange = (event, index) => {
        const { value } = event.target;
        const ingredients = [...recipe.ing];
        ingredients[index] = value;
        setRecipe({ ...recipe, 'ing': ingredients });
    };

    const handleAddIngredient = () => {
        const ingredients = [...recipe.ing, ""];
        setRecipe({ ...recipe, 'ing': ingredients });
        console.log(recipe);
    };

    const handleCreate = () => {
        // Requesti per backun
        axios.post('http://localhost:3001/recipes/', {
            name: recipe.name,
            ingredients: recipe.ing,
            instructions: recipe.ins,
            image_url: recipe.photo_url,
            cookingTime: recipe.time,
            userOwner: decodedToken.user._id,
        })
        .then(function (response) {
            console.log("Inside Axios success callback");
            console.log(response.data.data);
            if (response.data.data) {
                setCookies("token", response.data);
            }
        })
        .catch(function (error) {
            alert('Kredencialet gabim');
            console.log(error);
        });
        navigator("/home");
    }

    return (
        <div className='flex justify-center p-5 pb-[10%]'>
            <form className="wrapper">
                <h2>Create recipe</h2>
                <section className="group">
                    <input
                        type="text"
                        size="30"
                        className="input"
                        name="name"
                        required
                        onChange={handleChange}
                    />
                    <label htmlFor="name" className="label">
                        Name
                    </label>
                </section>
                <section className="group">
                    <textarea
                        type="text"
                        size="30"
                        className="input"
                        name="desc"
                        required
                        onChange={handleChange}
                    />
                    <label htmlFor="desc" className="label">
                        Description
                    </label>
                </section>
                <section className="group">
                    <input
                        type="number"
                        className="input"
                        name="time"
                        min={1}
                        required
                        onChange={handleChange}
                    />
                    <label htmlFor="time" className="label">
                        Time (in mins)
                    </label>
                </section>
                <section className="group">
                    <label htmlFor="ing">Ingredients</label>
                    {recipe.ing.map((ing, index) => (
                        <input
                            key={index}
                            className="input"
                            type="text"
                            name="ing"
                            value={ing}
                            onChange={(event) => handleIngredientChange(event, index)}
                        />
                    ))}
                    <button type="button" onClick={handleAddIngredient}>
                        Add Ingredient
                    </button>
                </section>
                <section className="group">
                    <input
                        type="text"
                        className="input"
                        name="photo_url"
                        required
                        onChange={handleChange}
                    />
                    <label htmlFor="photo_url" className="label">
                        Photo URL
                    </label>
                </section>
                <section className="group">
                    <input
                        type="text"
                        className="input"
                        name="ins"
                        required
                        onChange={handleChange}
                    />
                    <label htmlFor="ins" className="label">
                        Instructions
                    </label>
                </section>
                <button type="button" className="btn" onClick={handleCreate}>
                    Save recipe
                </button>
                <span className="form_shadow"></span>
            </form>
        </div>
    )
}

export default CreateRecipe