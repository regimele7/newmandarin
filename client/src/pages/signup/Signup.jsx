import React, { useState } from 'react'
import axios from 'axios'
import { useNavigate } from "react-router-dom"
import { useCookies } from "react-cookie"
import './signup.css'

const Signup = () => {
  const [_, setCookies] = useCookies(["name"]);
  const [userName, setUserName] = useState("");
  const [name, setName] = useState("");
  const [lName, setLName] = useState("");
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");
  const [confirmPass, setConfirmPass] = useState("");
  const [dt, setDt] = useState();
  
  const [sot, setSot] = new useState(new Date());
  const navigate = useNavigate();

  const handleSubmit = (event) => {

    event.preventDefault();
    if (name == '' || email == '' || pass == '' || lName == '' || confirmPass == '') {
      return
    }

    // Requesti per backun
    axios.post('http://localhost:3001/auth/register',{
      username: userName,
      password: pass,
      name: name,
      last_name: lName,
      birth_date: dt
    })
    .then(function (response) {
      console.log("Inside Axios success callback");
      console.log(response.response.data);
      if (response.response.data.token) {
         setCookies("token", response.response.data.token);
      }
    })
    .catch(function (error) {
      console.log(error);
    });

    window.localStorage.setItem("name", name);
    window.localStorage.setItem("email", email);
    navigate("/home");
  }


  return (
    <div className='flex justify-center p-5 pb-[10%]'>
      <form className="wrapper">
        <h2>LOGIN</h2>
        <section className="group">
          <input
            type="text"
            size="30"
            className="input"
            name="username"
            required
            onChange={(event) => setUserName(event.target.value)}
          />
          <label htmlFor="name" className="label">
            Username
          </label>
        </section>
        <section className="group">
          <input
            type="text"
            size="30"
            className="input"
            name="name"
            required
            onChange={(event) => setName(event.target.value)}
          />
          <label htmlFor="name" className="label">
            First name
          </label>
        </section>
        <section className="group">
          <input
            type="text"
            size="30"
            className="input"
            name="name"
            required
            onChange={(event) => setLName(event.target.value)}
          />
          <label htmlFor="name" className="label">
            Last name
          </label>
        </section>
        <section className="group">
          <input
            type="date"
            className="input"
            name="dt"
            required
            min={new Date(1900, 0, 1).toJSON().slice(0, 10)}
            max={new Date((sot.getFullYear()-18), 0, 1).toJSON().slice(0, 10)}
            onChange={(event) => setDt(event.target.value)}
          />
          <label htmlFor="dt" className="label">
            Datelindje
          </label>
        </section>
        <section className="group">
          <input
            type="text"
            size="30"
            className="input"
            name="email"
            required
            onChange={(event) => setEmail(event.target.value)}
          />
          <label htmlFor="email" className="label">
            Email
          </label>
        </section>
        <section className="group">
          <input
            type="password"
            minLength="8"
            className="input"
            name="password1"
            required
            onChange={(event) => setPass(event.target.value)}
          />
          <label htmlFor="password1" className="label">
            Password
          </label>
        </section>
        <section className="group">
          <input
            type="password"
            minLength="8"
            className="input"
            name="password2"
            required
            onChange={(event) => setConfirmPass(event.target.value)}
          />
          <label htmlFor="password2" className="label">
            Password
          </label>
        </section>
        <button type="button" className="btn" onClick={handleSubmit}>
          Sign up
        </button>
        <span className="form_shadow"></span>
      </form>
    </div>
  )
}

export default Signup