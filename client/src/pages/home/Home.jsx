import React, {useEffect, useState} from 'react';
import axios from 'axios';
import Box from '../../components/box/Box';
import HTMLFlipBook from 'react-pageflip';

const Home = () => {
  const [ recipes, setRecipes ] = useState([]);

  useEffect(() => {
    function fetchData() {
      axios.get('http://localhost:3001/recipes/')
        .then(function (response) {
          return response.data
        })
        .then(data => {
          setRecipes(currentRecipes => [...recipes, ...data]);
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    fetchData();
  }, [])
  
  return (
    // <Header/>
    // <Content/>
    // <SubFooter/>
    <div className='w-full flex justify-center bg-gray-400 '>
      <div className='w-full h-full p-10 grid grid-cols-3 gap-x-2	gap-y-10	'>
          {
            recipes.map(recete=>{
              return (
                <Box key={recete._id} recipt_id={recete._id} title={recete.name} desc={recete.instructions.slice(0,100)+"..."} ing={recete.ingredients} photo_url={recete.image_url} /> 
              )
            })
          }
      </div>
    </div>
  )
}

export default Home





{/*         
      <HTMLFlipBook width={300} height={300}>
          <div className="demoPage">
            <img className="w-full flex justify-center items-center" src='https://www.pergatit.com/wp-content/uploads/2019/09/Embelsira-750x500.jpg' alt="Sunset in the mountains"/>
          </div>
          <div className="demoPage">
            <div>
              <h1>Embelsire</h1>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt, vero accusantium. Illo tenetur quam, laboriosam, molestias quas, cum ad veritatis totam vel porro est distinctio. Consectetur, asperiores. Explicabo, temporibus sapiente!
              </p>
              <ul>
                <li>Uje</li>
                <li>Miell</li>
                <li>Sheqer</li>
              </ul>
            </div>
          </div>
        </HTMLFlipBook> 
*/}
