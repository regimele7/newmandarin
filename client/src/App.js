import { Route, Routes } from "react-router-dom";

import Home from "./pages/home/Home";
import About from "./pages/about/About";
import Testime from "./pages/testime/Testime";

import Footer from "./components/footer/Footer";
import Navbar from "./components/navbar/Navbar";
import Login from "./pages/login/Login";
import Signup from "./pages/signup/Signup";
import CreateRecipe from "./pages/create-recipe/CreateRecipe";
import SingleRecipe from "./pages/single-recipe/SingleRecipe";

function App() {
  return (
    <>
      <Navbar/>
        <Routes>
          <Route path="" element={<Home />} />
          <Route path="/home" element={<Home />} />
          <Route path="/about" element={<About />} />
          <Route path="/login" element={<Login />} />
          <Route path="/signup" element={<Signup />} />
          <Route path="/single-recipe" element={<SingleRecipe />} />
          <Route path="/create-recipe" element={<CreateRecipe />} />
          {/* <Route path="/auth" element={<Auth />} /> */}
          <Route path="/testime" element={<Testime/>} />
        </Routes>
      <Footer/>
    </>
  );
}

export default App;
