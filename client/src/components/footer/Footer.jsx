import React from 'react'
import { Link } from 'react-router-dom'
import { CiFacebook } from "react-icons/ci";
import { FaInstagram } from "react-icons/fa";
import { BsTwitterX } from "react-icons/bs";
import { FaTiktok } from "react-icons/fa";

const Footer = () => {
    return (
        <>
            <div className="flex justify-between px-[200px] py-[10px] bg-[#222831] text-white">
                <div className="w-[100px]"> <img src="https://freesvg.org/img/johnny_automatic_garlic_1.png" alt="garlic image" /></div>
                <div className="flex flex-col justify-around w-[55%] text-[20px] items-center">
                    <Link to='/home'>Home</Link>
                    <Link to='/about'>About</Link>
                    <Link to='/home'>Home</Link>
                    <Link to='/about'>About</Link>
                </div>
                <div className="flex flex-col justify-around w-[55%] text-[20px] items-center">
                    <div>Adresa 1</div>
                    <div>Adresa 1</div>
                    <div>Adresa 1</div>
                    <div>Adresa 1</div>
                </div>
                <div className="flex flex-col justify-around w-[55%] text-[20px] items-center">
                    <div className='flex items-center'><CiFacebook />Emri Faqes</div>
                    <div className='flex items-center'><FaInstagram />@username_faqes</div>
                    <div className='flex items-center'><BsTwitterX />@Username_twitter</div>
                    <div className='flex items-center'><FaTiktok /> @tiktok_username</div>
                </div>
            </div>
            <div className='flex justify-center bg-[#222831] text-white'>Copyright ©</div>
        </>
    )
}

export default Footer