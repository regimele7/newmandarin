import React, { useState, useEffect, componentDidMount } from 'react';
import { Link } from 'react-router-dom';
import { useNavigate } from "react-router-dom";
import { useCookies } from "react-cookie";

const Navbar = () => {
    const [name, setName] = useState(window.localStorage.getItem("name"));
    const [cookies, setCookies] = useCookies(["name"]);

    const navigate = useNavigate();

    const handleLogOut = () => {
        setCookies("token", "");
        window.localStorage.clear();
        navigate("/login");
    }

    return (
        <>
            <div className="flex justify-between px-[200px] py-[10px] bg-[#222831] text-white">
                <div className="w-[100px]"> <img src="https://freesvg.org/img/johnny_automatic_garlic_1.png" alt="garlic image" /></div>
                <div className="flex justify-around w-[55%] text-[20px] items-center">
                    <Link to='/home'>Home</Link>
                    <Link to='/about'>About</Link>
                    {
                        cookies.token ?
                            <>
                                <Link to='/create-recipe'>Create Receipes</Link>
                                <h3>My Saved Receipes</h3>
                                <h1>Hello {name}</h1>
                                <button type="button" className="btn" onClick={handleLogOut}>
                                    LOGOUT
                                </button>
                            </>
                            :
                            <>
                                <Link to='/login'>Log in</Link>
                                <Link to='/signup'>Sign up</Link>
                            </>
                    }
                </div>
            </div>
        </>
    )
}

export default Navbar