import React, { useState } from 'react'
import axios from 'axios';
import { useCookies } from "react-cookie";
import { useJwt } from "react-jwt";
import { useNavigate } from "react-router-dom";

const Box = ({recipt_id, title, desc, ing, photo_url}) => {
    const [saved, setSaved] = useState(false);
    const [cookies, setCookies] = useCookies(["name"]);
    const { decodedToken, isExpired } = useJwt(cookies.token);
    const navigator = useNavigate();

    function handleSaveRecipe() {
        setSaved(true);
        axios.put('http://localhost:3001/recipes/saveRecipe',{
            recipeID: recipt_id,
            userID: decodedToken.user._id,
        })
    }

    function handleClick() {
        navigator(
            '/single-recipe/',{
          state: {
            recipt_id
          }
        })
    }

    return (
        <div onClick={handleClick} className='flex justify-center'>
        <div className=" max-w-sm rounded-lg overflow-hidden shadow-lg bg-white">
            <img className="w-full" src={photo_url} alt="Sunset in the mountains"/>
                <div className="px-6 py-4">
                    <div className="flex justify-between font-bold text-xl mb-2">
                        <div>{title}</div>
                        {
                            saved ?
                            <img onClick={() => setSaved(false)} width="30px" src="https://cdn.iconscout.com/icon/free/png-256/free-save-1912198-1617667.png" alt="" />
                            :
                            <img onClick={handleSaveRecipe} width="30px" src="https://cdn.iconscout.com/icon/free/png-256/free-save-3244517-2701888.png?f=webp" alt="" />
                        }
                    </div>
                    <p className="text-gray-700 text-base">
                        {desc}
                    </p>
                </div>
                <div className="px-6 pt-4 pb-2">
                    {
                        ing.slice(0,3).map(i => {
                            return(
                                <span key={i} className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#{i}</span>
                            )
                        })
                    }
                </div>
        </div>
    </div>
    )
}

export default Box